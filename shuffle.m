function [Xl, Tl, Xt, Tt, N, Nl, Nt] = shuffle(X,T,perc)
    if (nargin < 3)
        perc = 0.8;
    end;
    [N,~] = size(X);
    idx = randperm(N);
    
    Nl = floor(N*perc);    
    Xl = X(idx(1:Nl),:);
    Tl = T(idx(1:Nl),:);
    Xt = X(idx(Nl+1:end),:);
    Tt = T(idx(Nl+1:end),:);
    
    Nt = N - Nl;
end
